document.addEventListener('DOMContentLoaded', () => {
    'use strict';

    const movieDB = {
        movies: [
            "Логан",
            "Лига справедливости",
            "Ла-ла лэнд",
            "Одержимость",
            "Скотт Пилигрим против..."
        ]
    };

    const adv = document.querySelectorAll('.promo__adv img');
    const poster = document.querySelector('.promo__bg');
    const genre = poster.querySelector('.promo__genre');
    const movieList = document.querySelector('.promo__interactive-list');
    const addForm = document.querySelector('form.add');
    const addInput = addForm.querySelector('.adding__input');
    const chekbox = addForm.querySelector('[type="checkbox"]');

    function removeAdv() {
        adv.forEach(item => {
            item.remove();
        });
    };

    function movieAdd(list, arr) {
        list.innerHTML = '';
        sortArr();

        arr.forEach((el, idx) => {
            list.innerHTML += `
            <li class="promo__interactive-item">${idx+1}) ${el}
            <div class="delete"></div>
            </li>
            `;
        });

        document.querySelectorAll('.delete').forEach((btn, idx) => {
            btn.addEventListener('click', () => {
                btn.parentElement.remove();
                arr.splice(idx, 1);
                movieAdd(list, arr);
            });
        });
    };

    function decorate() {
        genre.textContent = 'Драма';
        poster.style.backgroundImage = 'url("./img/bg.jpg")';
    }

    function sortArr() {
        movieDB.movies.sort();
    }

    addForm.addEventListener('submit', (event) => {
        event.preventDefault();

        let newFilm = addInput.value;
        const favorite = chekbox.checked;

        if (newFilm) {

            if (newFilm.length > 21) {
                newFilm = `${newFilm.slice(0,22)}...`;
            }
            if (favorite) {
                console.log('Добавляем любимый фильм');
            }
            movieDB.movies.push(newFilm);
            sortArr();

            movieAdd(movieList, movieDB.movies);

        };
        event.target.reset();
    })

    decorate();
    removeAdv();
    movieAdd(movieList, movieDB.movies);
    
    });


