"use strict";

let personalMovieDB = {
    count: 0,
    movies: {},
    actors: {},
    genres: [],
    privat: false,
    start: function() {
        personalMovieDB.count = +prompt("Сколько фильмов вы уже посмотрели", "");
        
        while (personalMovieDB.count == '' 
        || personalMovieDB.count == null 
        || isNaN(personalMovieDB.count)) {
            personalMovieDB.count = +prompt("Сколько фильмов вы уже посмотрели", "");
        }
    },
    rememberMyFilms: function() {

        for (let i = 0; i < 2; i++) {
        
            let a = prompt('Один из последних просмотренных фильмов', ''),
                b = prompt('На сколько оцените его?', '');
        
            if (
                a != null && b !=null 
                && a != '' && b != '' 
                && a.length < 50 && b.length < 50
                ) {
                personalMovieDB.movies[a] = b;
                console.log('Done');
            } else i--;
           
        };
        
    },
    detectPersonalLevel: function() {

        if (personalMovieDB.count < 10) {
            console.log("Просмотрено довольно мало фильмов");
        } else if (personalMovieDB.count < 30 
            && personalMovieDB.count > 10) {
            console.log("Вы классический зритель");
        } else if (personalMovieDB.count > 30) {
            console.log("Вы киноман");
        } else {
            console.log("Что-то пошло не так!");
        }
    
    },
    showMyDB: function(hidden) {
        if (!hidden) {
            console.log(personalMovieDB);
        } 
    },
    writeYourGenres: function() {
        for (let i = 1; i <= 3; i++) {
            let a = prompt(`Ваш любимый жанр под номером ${i}`);
            
            (a != null && a!= '') ? 
            personalMovieDB.genres.push(a) : 
            i--;
                
        }
        personalMovieDB.genres.forEach((el, idx) => 
            console.log(`Любимый жанр #${idx+1} - это ${el}`))
    },
    toggleVisibleMyDB: function() {
        personalMovieDB.privat = !personalMovieDB.privat;
        // if (personalMovieDB.privat) {
        //     personalMovieDB.privat = false;
        // } else {
        //     personalMovieDB.privat = true;
        // }
    }
};

personalMovieDB.start();
personalMovieDB.rememberMyFilms();
personalMovieDB.detectPersonalLevel();
personalMovieDB.showMyDB(personalMovieDB.privat);
personalMovieDB.writeYourGenres();
personalMovieDB.toggleVisibleMyDB();



















